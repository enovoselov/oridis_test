# Установка
- Клонируем репозиторий;
- Добавляем в композер глобальный пакет ``` php composer.phar global require "fxp/composer-asset-plugin:~1.1.1" ```
- В директории ``` oridis_test/site ``` запускаем инсталяцию композера ``` php composer.phar install ```;
- Настраиваем подключение к базе данных в ``` common/config/main-local.php ```
- Запускаем миграцию бд ``` php yii migrate ```
- Запускаем фикстуры тестовых данных ``` php yii fixture/load '*' --namespace='console\fixtures' ```
