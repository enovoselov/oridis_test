<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParameterPaste */

$this->title = 'Create Parameter Paste';
$this->params['breadcrumbs'][] = ['label' => 'Parameter Pastes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parameter-paste-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
