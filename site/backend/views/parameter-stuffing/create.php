<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParameterStuffing */

$this->title = 'Create Parameter Stuffing';
$this->params['breadcrumbs'][] = ['label' => 'Parameter Stuffings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parameter-stuffing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
