<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParameterTarget */

$this->title = 'Create Parameter Target';
$this->params['breadcrumbs'][] = ['label' => 'Parameter Targets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parameter-target-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
