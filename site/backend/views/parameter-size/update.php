<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ParameterSize */

$this->title = 'Update Parameter Size: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Parameter Sizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parameter-size-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
