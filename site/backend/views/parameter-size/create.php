<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParameterSize */

$this->title = 'Create Parameter Size';
$this->params['breadcrumbs'][] = ['label' => 'Parameter Sizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parameter-size-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
