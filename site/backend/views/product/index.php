<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'slug',
            'content:ntext',
            'price',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'parameter_size_id',
            // 'parameter_stuffing_id',
            // 'parameter_target_id',
            // 'parameter_paste_id',
            // 'parameter_oven_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
