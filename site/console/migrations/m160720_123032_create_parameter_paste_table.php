<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parameter_paste`.
 */
class m160720_123032_create_parameter_paste_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%parameter_paste}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%parameter_paste}}');
    }
}
