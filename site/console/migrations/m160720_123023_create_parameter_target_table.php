<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parameter_target`.
 */
class m160720_123023_create_parameter_target_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%parameter_target}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%parameter_target}}');
    }
}
