<?php

use yii\db\Migration;

/**
 * Handles adding parameter to table `product`.
 */
class m160722_090833_add_parameter_columns_to_product_table extends Migration
{
    private $tableName = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->tableName, 'parameter_size_id', $this->integer());

        // creates index for column `parameter_size_id`
        $this->createIndex(
            'idx-product-parameter_size_id',
            $this->tableName,
            'parameter_size_id'
        );

        // add foreign key for table `parameter_size`
        $this->addForeignKey(
            'fk-product-parameter_size_id',
            $this->tableName,
            'parameter_size_id',
            '{{%parameter_size}}',
            'id',
            'SET NULL',
            'RESTRICT'
        );

        $this->addColumn($this->tableName, 'parameter_stuffing_id', $this->integer());

        // creates index for column `parameter_stuffing_id`
        $this->createIndex(
            'idx-product-parameter_stuffing_id',
            $this->tableName,
            'parameter_stuffing_id'
        );

        // add foreign key for table `parameter_stuffing`
        $this->addForeignKey(
            'fk-product-parameter_stuffing_id',
            $this->tableName,
            'parameter_stuffing_id',
            '{{%parameter_stuffing}}',
            'id',
            'SET NULL',
            'RESTRICT'
        );

        $this->addColumn($this->tableName, 'parameter_target_id', $this->integer());

        // creates index for column `parameter_target_id`
        $this->createIndex(
            'idx-product-parameter_target_id',
            $this->tableName,
            'parameter_target_id'
        );

        // add foreign key for table `parameter_target`
        $this->addForeignKey(
            'fk-product-parameter_target_id',
            $this->tableName,
            'parameter_target_id',
            '{{%parameter_target}}',
            'id',
            'SET NULL',
            'RESTRICT'
        );

        $this->addColumn($this->tableName, 'parameter_paste_id', $this->integer());

        // creates index for column `parameter_paste_id`
        $this->createIndex(
            'idx-product-parameter_paste_id',
            $this->tableName,
            'parameter_paste_id'
        );

        // add foreign key for table `parameter_paste`
        $this->addForeignKey(
            'fk-product-parameter_paste_id',
            $this->tableName,
            'parameter_paste_id',
            '{{%parameter_paste}}',
            'id',
            'SET NULL',
            'RESTRICT'
        );

        $this->addColumn($this->tableName, 'parameter_oven_id', $this->integer());

        // creates index for column `parameter_oven_id`
        $this->createIndex(
            'idx-product-parameter_oven_id',
            $this->tableName,
            'parameter_oven_id'
        );

        // add foreign key for table `parameter_oven`
        $this->addForeignKey(
            'fk-product-parameter_oven_id',
            $this->tableName,
            'parameter_oven_id',
            '{{%parameter_oven}}',
            'id',
            'SET NULL',
            'RESTRICT'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `parameter_size`
        $this->dropForeignKey(
            'fk-product-parameter_size_id',
            $this->tableName
        );

        // drops index for column `parameter_size_id`
        $this->dropIndex(
            'idx-product-parameter_size_id',
            $this->tableName
        );

        $this->dropColumn($this->tableName, 'parameter_size_id');

        // drops foreign key for table `parameter_stuffing`
        $this->dropForeignKey(
            'fk-product-parameter_stuffing_id',
            $this->tableName
        );

        // drops index for column `parameter_stuffing_id`
        $this->dropIndex(
            'idx-product-parameter_stuffing_id',
            $this->tableName
        );

        $this->dropColumn($this->tableName, 'parameter_stuffing_id');

        // drops foreign key for table `parameter_target`
        $this->dropForeignKey(
            'fk-product-parameter_target_id',
            $this->tableName
        );

        // drops index for column `parameter_target_id`
        $this->dropIndex(
            'idx-product-parameter_target_id',
            $this->tableName
        );

        $this->dropColumn($this->tableName, 'parameter_target_id');

        // drops foreign key for table `parameter_paste`
        $this->dropForeignKey(
            'fk-product-parameter_paste_id',
            $this->tableName
        );

        // drops index for column `parameter_paste_id`
        $this->dropIndex(
            'idx-product-parameter_paste_id',
            $this->tableName
        );

        $this->dropColumn($this->tableName, 'parameter_paste_id');

        // drops foreign key for table `parameter_oven`
        $this->dropForeignKey(
            'fk-product-parameter_oven_id',
            $this->tableName
        );

        // drops index for column `parameter_oven_id`
        $this->dropIndex(
            'idx-product-parameter_oven_id',
            $this->tableName
        );

        $this->dropColumn($this->tableName, 'parameter_oven_id');
    }
}
