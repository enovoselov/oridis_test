<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parameter_oven`.
 */
class m160720_123042_create_parameter_oven_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%parameter_oven}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%parameter_oven}}');
    }
}
