<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parameter_size`.
 */
class m160720_122946_create_parameter_size_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%parameter_size}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%parameter_size}}');
    }
}
