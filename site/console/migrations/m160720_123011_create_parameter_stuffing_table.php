<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parameter_stuffing`.
 */
class m160720_123011_create_parameter_stuffing_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%parameter_stuffing}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%parameter_stuffing}}');
    }
}
