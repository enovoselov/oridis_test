<?php

namespace console\fixtures;

use yii\test\ActiveFixture;

class ParameterOvenFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterOven';
    public $dataFile = '@app/fixtures/data/parameter_oven.php';
}