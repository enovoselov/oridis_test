<?php

namespace console\fixtures;

use yii\test\ActiveFixture;

class ParameterPasteFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterPaste';
    public $dataFile = '@app/fixtures/data/parameter_paste.php';
}