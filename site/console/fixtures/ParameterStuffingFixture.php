<?php

namespace console\fixtures;

use yii\test\ActiveFixture;

class ParameterStuffingFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterStuffing';
    public $dataFile = '@app/fixtures/data/parameter_stuffing.php';
}