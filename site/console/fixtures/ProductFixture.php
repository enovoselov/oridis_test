<?php

namespace console\fixtures;

use yii\test\ActiveFixture;

class ProductFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Product';
    public $dataFile = '@app/fixtures/data/product.php';
    public $depends = [
        'console\fixtures\ParameterSizeFixture',
        'console\fixtures\ParameterStuffingFixture',
        'console\fixtures\ParameterTargetFixture',
        'console\fixtures\ParameterPasteFixture',
        'console\fixtures\ParameterOvenFixture',
    ];
}