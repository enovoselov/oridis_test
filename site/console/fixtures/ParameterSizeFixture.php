<?php

namespace console\fixtures;

use yii\test\ActiveFixture;

class ParameterSizeFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterSize';
    public $dataFile = '@app/fixtures/data/parameter_size.php';
}