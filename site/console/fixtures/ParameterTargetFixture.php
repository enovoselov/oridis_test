<?php

namespace console\fixtures;

use yii\test\ActiveFixture;

class ParameterTargetFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ParameterTarget';
    public $dataFile = '@app/fixtures/data/parameter_target.php';
}