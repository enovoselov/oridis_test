<?php

return [
    ['id' => 1, 'name' => 'Малина', 'slug' => 'malina'],
    ['id' => 2, 'name' => 'Клубника', 'slug' => 'klubnika'],
    ['id' => 3, 'name' => 'Смородина', 'slug' => 'smorodina'],
    ['id' => 4, 'name' => 'Творог', 'slug' => 'tvorog'],
    ['id' => 5, 'name' => 'Изюм', 'slug' => 'izum'],
    ['id' => 6, 'name' => 'Капуста', 'slug' => 'kapusta'],
    ['id' => 7, 'name' => 'Яйцо', 'slug' => 'ajco'],
    ['id' => 8, 'name' => 'Яблоко', 'slug' => 'abloko'],
    ['id' => 9, 'name' => 'Мед', 'slug' => 'med'],
    ['id' => 10, 'name' => 'Баранина', 'slug' => 'baranina'],
    ['id' => 11, 'name' => 'Свинина', 'slug' => 'svinina'],
    ['id' => 12, 'name' => 'Картошка', 'slug' => 'kartoska'],
];