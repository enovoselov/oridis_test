<?php

$productsArray = [];

for ($i = 1; $i < 100101; $i++) {
    $productsArray[] = [
        'id' => $i,
        'name' => 'Продукт-' . $i,
        'slug' => 'product-' . $i,
        'content' => 'Lorem ipsum dolor sit amet.',
        'price' => rand(10, 1000),
        'status' => 10,
        'created_at' => '1392559490',
        'updated_at' => '1392559490',
        'parameter_size_id' => rand(1, 4),
        'parameter_stuffing_id' => rand(1, 12),
        'parameter_target_id' => rand(1, 4),
        'parameter_paste_id' => rand(1, 13),
        'parameter_oven_id' => rand(1, 3)
    ];
}

return $productsArray;