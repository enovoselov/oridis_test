<?php

return [
    ['id' => 1, 'name' => 'Сытный', 'slug' => 'sytnyj'],
    ['id' => 2, 'name' => 'Легкий', 'slug' => 'legkij'],
    ['id' => 3, 'name' => 'Подарочный', 'slug' => 'podarocnyj'],
    ['id' => 4, 'name' => 'Спортивный', 'slug' => 'sportivnyj'],
];