<?php

return [
    ['id' => 1, 'name' => 'Слоистое', 'slug' => 'sloistoe'],
    ['id' => 2, 'name' => 'Дрожжевое', 'slug' => 'drozzevoe'],
    ['id' => 3, 'name' => 'Ржаное', 'slug' => 'rzanoe'],
    ['id' => 4, 'name' => 'Бездрожжевое', 'slug' => 'bezdrozzevoe'],
    ['id' => 5, 'name' => 'Песочное', 'slug' => 'pesocnoe'],
    ['id' => 6, 'name' => 'Бисквитное', 'slug' => 'biskvitnoe'],
    ['id' => 7, 'name' => 'Блинное', 'slug' => 'blinnoe'],
    ['id' => 8, 'name' => 'Лапшевое', 'slug' => 'lapsevoe'],
    ['id' => 9, 'name' => 'Заварное', 'slug' => 'zavarnoe'],
    ['id' => 10, 'name' => 'Белковое', 'slug' => 'belkovoe'],
    ['id' => 11, 'name' => 'Миндальное', 'slug' => 'mindalnoe'],
    ['id' => 12, 'name' => 'Кукурузное', 'slug' => 'kukuruznoe'],
    ['id' => 13, 'name' => 'Овсяное', 'slug' => 'ovsanoe'],
];