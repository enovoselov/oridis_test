<?php

return [
    ['id' => 1, 'name' => 'Огромные', 'slug' => 'ogromnye'],
    ['id' => 2, 'name' => 'Большие', 'slug' => 'bolsie'],
    ['id' => 3, 'name' => 'Средние', 'slug' => 'srednie'],
    ['id' => 4, 'name' => 'Маленькие', 'slug' => 'malenkie'],
];