<?php

namespace frontend\controllers;

use common\components\ParametersMenuItem;
use common\models\Product;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class CatalogController extends \yii\web\Controller
{
    public function actionIndex($slug = null)
    {
        $verifySlug = $this->verifyIndexSlug($slug);
        if ($slug !== null && $slug != $verifySlug) {
            if ($verifySlug == ''){
                return $this->redirect(['index']);
            }
            return $this->redirect(['index', 'slug' => $verifySlug]);
        }

        $productsQuery = Product::find()->active()->forParameters($slug)->orderBy(['id' => SORT_DESC]);

        $productsDataProvider = new ActiveDataProvider([
            'query' => $productsQuery,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->view->params['slug'] = $slug;
        $this->view->params['printParameterMenu'] = true;

        return $this->render('index', [
            'productsDataProvider' => $productsDataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionProduct($id)
    {
        return $this->render('product', [
            'model' => $this->findProductModel($id),
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProductModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Функция проверяю путь в каталоге и возвращает верный на основе структуры пути
     * @param $slug
     * @return mixed|string
     */
    protected function verifyIndexSlug($slug)
    {
        $key = 'verifyIndexSlug-' . $slug;

        $result = \Yii::$app->cache->get($key);

        if ($result === false)
        {
            $menuItems = ParametersMenuItem::getMenuItemsArray();

            $structureLinkArray = ParametersMenuItem::getStructureLinkArray();

            $structureLinkArray = ParametersMenuItem::setStructureLinkArray($structureLinkArray, $menuItems, $slug);

            $result = implode('/', array_diff($structureLinkArray, array('')));

            \Yii::$app->cache->set($key, $result);
        }

        return $result;
    }
}
