<?php

namespace frontend\widgets;


use common\components\ParametersMenuItem;
use common\models\Product;
use yii\base\Widget;
use yii\helpers\Url;

class ParameterMenu extends Widget
{
    /**
     * Содержит данные меню
     * @var array
     */
    public $menuArray = [];

    /**
     * Содержит структуру url, для того чтобы выстроить путь в нужном порядке
     * @var array
     */
    public $structureLinkArray = [];

    /**
     * Содержит путь в каталоге
     * @var string
     */
    public $slug = '';

    /**
     * Инициализирую интерфейс виджета
     */
    public function init()
    {
        parent::init();

        $this->structureLinkArray = ParametersMenuItem::getStructureLinkArray();

        $this->menuArray = ParametersMenuItem::getMenuItemsArray();

        $this->setPrimaryStatusMenuItem();

        $this->setSuccessStatusMenuItem();

        $this->structureLinkArray = ParametersMenuItem::setStructureLinkArray($this->structureLinkArray, $this->menuArray, $this->slug);

        $this->generationLinkMenuItems();
    }

    /**
     * Рендерю виджет
     */
    public function run()
    {
        parent::run();

        return $this->render('parameter-menu', [
            'parameterMenu' => $this->menuArray,
        ]);
    }

    /**
     * Устанавливает ссылкам меню статус 'primary', для цвета
     */
    private function setPrimaryStatusMenuItem()
    {
        $productUniqueParameterItems = [];

        foreach ($this->menuArray as $menuItem) {
            $productUniqueParameterItems[$menuItem['productColumn']] = Product::find()
                ->select($menuItem['productColumn'])
                ->distinct()
                ->active()
                ->forParameters($this->slug)
                ->asArray()
                ->indexBy($menuItem['productColumn'])
                ->all();
        }

        foreach ($productUniqueParameterItems as $parameterColumn => $productParameterIds) {
            foreach ($productParameterIds as $parameterId) {
                if (current($parameterId) !== null) {
                    $this->menuArray[$parameterColumn]['items'][current($parameterId)]['status'] = 'primary';
                }
            }
        }
    }

    /**
     * Устанавливает ссылкам меню статус 'success', для цвета
     */
    private function setSuccessStatusMenuItem()
    {
        $slugArray = explode('/', trim($this->slug, '/'));

        foreach ($slugArray as $slugItem) {
            foreach ($this->menuArray as $keyItem => $menuItem) {
                foreach ($menuItem['items'] as $keyLink => $linkItem) {
                    if ($linkItem['slug'] == $slugItem) {
                        $this->menuArray[$keyItem]['items'][$keyLink]['status'] = 'success';

                        break 2;
                    }
                }
            }
        }
    }

    /**
     * Генерирую ссылки для меню параметров
     */
    private function generationLinkMenuItems()
    {
        foreach ($this->menuArray as $keyMenuItem => $menuItem) {
            foreach ($menuItem['items'] as $keyLink => $linkItem) {
                $newStructureLinkArray = $this->structureLinkArray;

                $newStructureLinkArray[$keyMenuItem] = $linkItem['slug'];

                if (isset($linkItem['status']) && $linkItem['status'] == 'success') {
                    $newStructureLinkArray[$keyMenuItem] = '';
                }

                $this->menuArray[$keyMenuItem]['items'][$keyLink]['linkStructure'] = $newStructureLinkArray;

                $link = implode('/', array_diff($newStructureLinkArray, array(''))) . '/';

                if ($link == '/') {
                    $this->menuArray[$keyMenuItem]['items'][$keyLink]['link'] = Url::toRoute('catalog/index', ['slug' => $link]);
                } else {
                    $this->menuArray[$keyMenuItem]['items'][$keyLink]['link'] = Url::toRoute('catalog/index') . $link;
                }
            }
        }
    }

}