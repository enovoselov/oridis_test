<?php
/* @var $this \yii\web\View */
?>
<div class="container">
    <div class="row">
        <?php
        foreach ($parameterMenu as $parameters) {
            ?>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $parameters['label']; ?></div>
                    <div class="panel-body">
                        <?php
                        foreach ($parameters['items'] as $parameter) {
                            ?>
                            <p>
                                <a class="btn btn-<?php echo (isset($parameter['status'])) ? $parameter['status'] : 'default'; ?>"
                                   href="<?php echo urldecode($parameter['link']); ?>"
                                   role="button">
                                    <?php echo $parameter['name']; ?>
                                </a>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>