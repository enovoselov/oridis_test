<?php
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $productsDataProvider \yii\data\ActiveDataProvider */

$this->title = 'Каталог';
?>
<h1>Каталог</h1>

<h3>Найдено: <?php echo $productsDataProvider->getTotalCount(); ?> товара.</h3>

<div class="container catalog">
    <div class="row">
        <div class="col-xs-12">
            <?= ListView::widget([
                'dataProvider' => $productsDataProvider,
                'itemView' => '_product-item',
                'layout' => "{pager}\n{items}",
            ]) ?>
        </div>
    </div>
</div>