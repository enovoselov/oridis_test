<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \common\models\Product */
?>
<div class="col-lg-3 col-sm-3">
    <div class="thumbnail text-center">
        <div class="caption">
            <h4><?php echo Html::a(Html::encode($model->name), ['product', 'id' => $model->id]); ?></h4>
            <p><?php echo Yii::$app->formatter->asNtext($model->content); ?></p>
            <div class="btn-group">
                <?php
                echo !empty($model->price) ? Html::a($model->price.' руб.', ['product', 'id' => $model->id], ['class' => 'btn btn-default']) : '';
                echo  Html::a('Открыть', ['product', 'id' => $model->id], ['class' => 'btn btn-primary']);
                ?>
            </div>
        </div>
    </div>
</div>
