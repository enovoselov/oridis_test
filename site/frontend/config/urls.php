<?php

return [
    'catalog/product' => 'catalog/product',
    'catalog/<slug:[\w\/-]+>' => 'catalog/index',
    'catalog' => 'catalog/index',

    '' => 'site/index',
    '<action>' => 'site/<action>',
];