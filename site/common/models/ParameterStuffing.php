<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%parameter_stuffing}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 *
 * @property Product[] $products
 */
class ParameterStuffing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parameter_stuffing}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['parameter_stuffing_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ParameterStuffingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ParameterStuffingQuery(get_called_class());
    }

    public static function parameterProductColumnName()
    {
        return 'parameter_stuffing_id';
    }

    public static function menuName()
    {
        return 'По начинке:';
    }
}
