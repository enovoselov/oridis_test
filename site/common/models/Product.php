<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $content
 * @property integer $price
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $parameter_size_id
 * @property integer $parameter_stuffing_id
 * @property integer $parameter_target_id
 * @property integer $parameter_paste_id
 * @property integer $parameter_oven_id
 *
 * @property ParameterOven $parameterOven
 * @property ParameterPaste $parameterPaste
 * @property ParameterSize $parameterSize
 * @property ParameterStuffing $parameterStuffing
 * @property ParameterTarget $parameterTarget
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'created_at', 'updated_at'], 'required'],
            [['content'], 'string'],
            [['price', 'status', 'created_at', 'updated_at', 'parameter_size_id', 'parameter_stuffing_id', 'parameter_target_id', 'parameter_paste_id', 'parameter_oven_id'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['parameter_oven_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParameterOven::className(), 'targetAttribute' => ['parameter_oven_id' => 'id']],
            [['parameter_paste_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParameterPaste::className(), 'targetAttribute' => ['parameter_paste_id' => 'id']],
            [['parameter_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParameterSize::className(), 'targetAttribute' => ['parameter_size_id' => 'id']],
            [['parameter_stuffing_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParameterStuffing::className(), 'targetAttribute' => ['parameter_stuffing_id' => 'id']],
            [['parameter_target_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParameterTarget::className(), 'targetAttribute' => ['parameter_target_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'content' => 'Content',
            'price' => 'Price',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'parameter_size_id' => 'Parameter Size ID',
            'parameter_stuffing_id' => 'Parameter Stuffing ID',
            'parameter_target_id' => 'Parameter Target ID',
            'parameter_paste_id' => 'Parameter Paste ID',
            'parameter_oven_id' => 'Parameter Oven ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterOven()
    {
        return $this->hasOne(ParameterOven::className(), ['id' => 'parameter_oven_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterPaste()
    {
        return $this->hasOne(ParameterPaste::className(), ['id' => 'parameter_paste_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterSize()
    {
        return $this->hasOne(ParameterSize::className(), ['id' => 'parameter_size_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterStuffing()
    {
        return $this->hasOne(ParameterStuffing::className(), ['id' => 'parameter_stuffing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterTarget()
    {
        return $this->hasOne(ParameterTarget::className(), ['id' => 'parameter_target_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ProductQuery(get_called_class());
    }
}
