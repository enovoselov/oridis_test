<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%parameter_oven}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 *
 * @property Product[] $products
 */
class ParameterOven extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parameter_oven}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['parameter_oven_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ParameterOvenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ParameterOvenQuery(get_called_class());
    }

    public static function parameterProductColumnName()
    {
        return 'parameter_oven_id';
    }

    public static function menuName()
    {
        return 'По приготовлению:';
    }
}
