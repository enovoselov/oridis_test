<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ParameterPaste]].
 *
 * @see \common\models\ParameterPaste
 */
class ParameterPasteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ParameterPaste[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ParameterPaste|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
