<?php

namespace common\models\query;
use common\models\ParameterOven;
use common\models\ParameterPaste;
use common\models\ParameterSize;
use common\models\ParameterStuffing;
use common\models\ParameterTarget;

/**
 * This is the ActiveQuery class for [[\common\models\Product]].
 *
 * @see \common\models\Product
 */
class ProductQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]=10');
    }

    /**
     * @param $slug
     * @return $this
     */
    public function forParameters($slug)
    {

        $parameters = explode('/', trim($slug, '/'));

        $parametersClass = [
            [
                'class' => ParameterSize::className(),
                'productColumn' => ParameterSize::parameterProductColumnName(),
            ],
            [
                'class' => ParameterStuffing::className(),
                'productColumn' => ParameterStuffing::parameterProductColumnName(),
            ],
            [
                'class' => ParameterTarget::className(),
                'productColumn' => ParameterTarget::parameterProductColumnName(),
            ],
            [
                'class' => ParameterPaste::className(),
                'productColumn' => ParameterPaste::parameterProductColumnName(),
            ],
            [
                'class' => ParameterOven::className(),
                'productColumn' => ParameterOven::parameterProductColumnName(),
            ],
        ];

        $productParametersForSlug = [];
        foreach ($parameters as $parameter)
        {
            foreach ($parametersClass as $parameterClass)
            {
                /* @var $parameterClass \yii\db\ActiveRecord */
                $findParameter = $parameterClass['class']::find()->where(['slug' => $parameter])->asArray()->one();
                if (!empty($findParameter))
                {
                    $productParametersForSlug[$parameterClass['productColumn']] = $findParameter['id'];
                    break;
                }
            }
        }

        return $this->andWhere($productParametersForSlug);
    }

    /**
     * @inheritdoc
     * @return \common\models\Product[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Product|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
