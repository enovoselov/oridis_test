<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%parameter_size}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 *
 * @property Product[] $products
 */
class ParameterSize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parameter_size}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['parameter_size_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ParameterSizeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ParameterSizeQuery(get_called_class());
    }

    public static function parameterProductColumnName()
    {
        return 'parameter_size_id';
    }

    public static function menuName()
    {
        return 'По размеру:';
    }
}
