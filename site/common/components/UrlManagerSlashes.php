<?php

namespace common\components;


use yii\web\UrlManager;

class UrlManagerSlashes extends UrlManager
{
    public function createUrl($params)
    {
        return $this->fixPathSlashes(parent::createUrl($params));
    }

    protected function fixPathSlashes($url)
    {
        return preg_replace('|\%2F|i', '/', $url);
    }
}