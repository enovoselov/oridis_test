<?php

namespace common\components;


use common\models\ParameterOven;
use common\models\ParameterPaste;
use common\models\ParameterSize;
use common\models\ParameterStuffing;
use common\models\ParameterTarget;

class ParametersMenuItem
{
    /**
     * Возвращает массив меню
     * @return array
     */
    public static function getMenuItemsArray()
    {
        $menuItems = [];

        $menuItems[ParameterSize::parameterProductColumnName()] = [
            'productColumn' => ParameterSize::parameterProductColumnName(),
            'label' => ParameterSize::menuName(),
            'items' => ParameterSize::find()->asArray()->indexBy('id')->all(),
        ];

        $menuItems[ParameterStuffing::parameterProductColumnName()] = [
            'productColumn' => ParameterStuffing::parameterProductColumnName(),
            'label' => ParameterStuffing::menuName(),
            'items' => ParameterStuffing::find()->asArray()->indexBy('id')->all(),
        ];

        $menuItems[ParameterTarget::parameterProductColumnName()] = [
            'productColumn' => ParameterTarget::parameterProductColumnName(),
            'label' => ParameterTarget::menuName(),
            'items' => ParameterTarget::find()->asArray()->indexBy('id')->all(),
        ];

        $menuItems[ParameterPaste::parameterProductColumnName()] = [
            'productColumn' => ParameterPaste::parameterProductColumnName(),
            'label' => ParameterPaste::menuName(),
            'items' => ParameterPaste::find()->asArray()->indexBy('id')->all(),
        ];

        $menuItems[ParameterOven::parameterProductColumnName()] = [
            'productColumn' => ParameterOven::parameterProductColumnName(),
            'label' => ParameterOven::menuName(),
            'items' => ParameterOven::find()->asArray()->indexBy('id')->all(),
        ];

        return $menuItems;
    }

    /**
     * Возвращает массив меню
     * @return array
     */
    public static function getStructureLinkArray()
    {
        $structureLink = [
            ParameterSize::parameterProductColumnName() => '',
            ParameterStuffing::parameterProductColumnName() => '',
            ParameterTarget::parameterProductColumnName() => '',
            ParameterPaste::parameterProductColumnName() => '',
            ParameterOven::parameterProductColumnName() => '',
        ];

        return $structureLink;
    }

    /**
     * Устанавливаю соответсвие положения параметра в пути с его url
     * @param array $structureLinkArray
     * @param array $menuArray
     * @param string $slug
     * @return array
     */
    public static function setStructureLinkArray($structureLinkArray, $menuArray, $slug = null)
    {
        $slugArray = explode('/', trim($slug, '/'));

        foreach ($structureLinkArray as $keyLink => $slug) {
            foreach ($slugArray as $keySlug => $slugItem) {
                foreach ($menuArray[$keyLink]['items'] as $menuItem) {
                    if ($menuItem['slug'] == $slugItem) {
                        $structureLinkArray[$keyLink] = $slugItem;
                        unset($slugArray[$keySlug]);
                        break 2;
                    }
                }
            }
        }

        return $structureLinkArray;
    }
}